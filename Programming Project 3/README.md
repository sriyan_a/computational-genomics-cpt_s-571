## Building
Make sure you can build c+14 code with gcc. Do the following in the project root directory:
```
make clean && make
```

## Usage
The program takes two .fasta files as input, one containing a single reference genome and another containing several sequences for reads. To run the program and map the reads to the reference genome, do the following in the project root directory:
```
./ReadMapper reference_genome.fasta reads_to_map.fasta > outfile.txt
```

Note: This program has been tested in windows with a Linux subsystem. If running natively, changes to the MeasureStats methods is required. No changes is required to run in Linux (or Mac).