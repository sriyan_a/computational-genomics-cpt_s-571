#ifndef IO_STUFF_H
#define IO_STUFF_H
#include <string>
#include <fstream>
#include <vector>

using std::string;
using std::ifstream;
using std::getline;
using std::vector;

typedef struct Sequence {
	string title;
	string content;
} Sequence;

int readInput(string finame, vector<Sequence> &out);
#endif
