#include "io.h"
#include <suffix_tree.h>
#include <MeasureStats.h>
#include <dp.h>
#include <iostream>
#include <vector>

#define IdX ((double)0.90)
#define CoverY ((double)0.80)
#define Match 1
#define Mismatch -2
#define H -5
#define G -1

using std::cout;
using std::endl;
using std::vector;
using namespace suffix_tree;

ScoreParams params;

typedef struct read_responce {
	int start;
	int end;
} read_responce;

typedef struct stat {
	int hits;
	int withreads;
	int aligns;
} stat;

read_responce * mapread(Tree *st, Sequence &ref_genome, stat *readstat, Sequence &s) {
	vector<int> L = st->FindLoc(s.content);
	readstat->aligns += L.size();

	if (L.size() > 0)
		readstat->withreads++;

	int best_i = -1;
	double best_coverage = 0;
	int start = 0;
	int end = 0;

	for (int j = 0; j < L.size(); j++) {
		int offset = L[j];

		if (offset < s.content.length()) offset = 0;
		else offset -= s.content.length();

		int length = 3 * s.content.length();
		if (length + offset > ref_genome.content.length())
			length -= ((length + offset) - ref_genome.content.length());

		Alignment *a = calculateLocalAlignment(
				ref_genome.content.c_str() + offset,
				length,
				ref_genome.title.c_str(),
				s.content.c_str(),
				s.content.length(),
				s.title.c_str(),
				&params
				);

		int alignlen = a->nmatch + a->nmismatch + a->ngap;
		double identity = (double)a->nmatch / ((double)alignlen);
		double coverage = ((double)s.content.length()) / (double)alignlen;

		if (identity > IdX && coverage > CoverY && coverage > best_coverage) {
			best_i = j;
			best_coverage = coverage;
			start = offset + a->mini;
			end = offset + a->maxi;
		}

		free(a->alignpath);
		free(a->grid.cells);
		free(a);
	}

	if (best_i != -1) {
		read_responce *r = new read_responce();
		r->start = start;
		r->end = end;
		return r;
	}

	return nullptr;
}

void mapreads(Tree *st, Sequence &ref_genome, vector<Sequence> &seqs, int beg, 
					int end, stat *out, vector<read_responce*> &read_responce) {
	for (int i = beg; i < end; i++)
		read_responce[i] = mapread(st, ref_genome, out, seqs[i]);
}

int main (int argc, char *argv[]) {
	if (argc < 3) {
		cout << "must pass at least two paramters\n" << endl;
		return 1;
	}

	stat readstat = {0};
	Tree *st = nullptr;
	vector<Sequence> ref_genome;
	readInput(argv[1], ref_genome);
	cout << "Read genome, length = " << ref_genome[0].content.length() << endl;

	vector<Sequence> sequences;
	readInput(argv[2], sequences);
	cout << "Read " << sequences.size() <<  " sequences..." << endl;

	params.match = Match;
	params.mismatch = Mismatch;
	params.h = H;
	params.g = G;

	auto output_time = new Measure();
	auto mapreads_time = new Measure();
	auto construct_time = new Measure();
	auto prepare_time = new Measure();
	auto total_time = new Measure();

	total_time->start();

	construct_time->start();
	st = new Tree(ref_genome[0].content, "");
	st->Build();
	construct_time->end();

	prepare_time->start();
	st->PrepareIndexArray();
	prepare_time->end();

	vector<read_responce*> ress(sequences.size());

	mapreads_time->start();
	mapreads(st, ref_genome[0], sequences, 0, sequences.size(), &readstat, ress);
	mapreads_time->end();

	std::ofstream outfile;
	outfile.open("MappingResults_Peach_Reference.txt", std::ofstream::out | std::ofstream::trunc);

	output_time->start();
	for (int i = 0; i < ress.size(); i++) {
		Sequence &s = sequences[i];
		if (ress[i] == nullptr)
			outfile << s.title << "\t\tNo hit found" << endl;
		else {
			outfile << s.title << "\t\tStart: " << ress[i]->start-1 << ", End: " 
				<< ress[i]->end-1 << endl;
			readstat.hits++;
		}
	}
	output_time->end();
	total_time->end();

	int aligns = readstat.aligns;
	int withreads = readstat.withreads;
	int hits = readstat.hits;

	cout << "Hit rate = " << 100 * (double)hits / (double)sequences.size() 
		<< " (" << hits << "/" << sequences.size() <<")" << endl; 
	cout << "Average alignments per read (total): " << (double)aligns/sequences.size() 
		<< " (" << aligns << "/" << sequences.size() <<")" << endl;
	cout << "Average alignments per read (with reads): " << (double)aligns/withreads << " (" 
		<< aligns << "/" << withreads << ")" << endl;
	cout << "Mapreads time = " << mapreads_time->total() << " seconds" << endl;
	cout << "Build Suffix Tree time = " << construct_time->total() << " seconds" << endl;
	cout << "Prepare time = " << prepare_time->total() << " seconds" << endl;
	cout << "Output time = " << output_time->total() << " seconds" << endl;
	cout << "Total time taken = " << total_time->total() << " seconds" << endl;

	return 0;
}
