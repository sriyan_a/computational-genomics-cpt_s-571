#ifndef SUFFIX_TREE_NODE_H_
#define SUFFIX_TREE_NODE_H_
#include <string>
#include <vector>

namespace suffix_tree {
	class Tree;

	class Node {
		friend Tree;
		public:
			Node(int id, Node *parent, int beg, int len);
			void AddChild(Node *node);
			Node *parent, *child, *sibling, *suffixLink;
			int StartLeafIndex, EndLeafIndex, beg, len, id, stringDepth;
	};
}
#endif
