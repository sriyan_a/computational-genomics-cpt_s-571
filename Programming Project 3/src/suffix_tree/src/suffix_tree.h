#ifndef SUFFIX_TREE_H
#define SUFFIX_TREE_H
#include <string>
#include "suffix_tree_node.h"

#define panic(x) \
	cout << __FILE__ << ":" << __LINE__ << " " << x << " \n"; exit(1);

#define XValue 25

namespace suffix_tree {

	class Tree {
		
		private:
			Node *alloc_buf, *root;
			int alloc_i, alphabetSize, nextNodeId, printCount, B_i;
			std::string alphabet, &input;

			//For tree statistics
			bool ranDFS;
			int nIntNodes, nLeaves;
			Node *lowestIntNode;
			
			Node * insert(Node *prevLeaf, int suffix);
			Node * nodeHopTraversal(Node *start, int beta, int len);
			Node * getChildWithFirstChar(Node *n, char c);
			void addChildToNode(Node *parent, Node *child);
			void replaceChild(Node *parent, Node *newChild);
			void recursiveEnumerateNodesDFS(Node *n);
			void recursiveEnumerateBWT(Node *n);
			Node * createNewNode(int id, Node *parent, int beg, int len);	
			Node * breakEdge(Node *n, int i);

		public:
			std::vector<char> B;
			int *A, nextIndex;
			Tree(std::string& input, std::string alphabet);
			void Build();
			void EnumerateNodesDFS();
			Node * FindMatchPath(Node *node, const char *s, int *extra);
			Node * findPath(Node *node, int suffix, int beg, int len);
			void EnumerateBWT();
			void DFS_PrepareRoot(Node *nd);
			void PrepareIndexArray();
			std::vector<int> FindLoc(std::string& r);
	};
}

#endif
