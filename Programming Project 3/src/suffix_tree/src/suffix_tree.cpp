#include "suffix_tree.h"
#include <strings.h>
#include <iostream>
#include <cstring>

using std::cout;
using std::endl;

namespace suffix_tree {

	Tree::Tree(std::string& input, std::string alphabet): input(input), alphabet(alphabet),
			B(input.length() + 1, 0), B_i(0), ranDFS(false), nIntNodes(1), nLeaves(0) {
		
		input += '$';
		size_t size = sizeof(Node) * input.length() * 2;
		alloc_buf = (Node*)malloc(size);
		bzero(alloc_buf, size);
		alloc_i = 0;

		nextNodeId = input.length();
		root = createNewNode(++nextNodeId, nullptr, 0, 0);
		lowestIntNode = root;
		root->suffixLink = root;
	}

	void Tree::Build() {
		Node *n = root;
		for (int i = 0; i < input.length(); i++) {
			n = insert(n, i);
		}
	}

	//*******************************************************************************
	//Find path first checks if there is a child. If there is no child, that means we
	//need to add a new node (child) at that point. If child exists, we check it's
	//edge length with our len, if it is greater then that means we can hop one node.
	//If the len is less than child's edge len then that means our suffix finishes
	//mid way somewhere along the edge. So we break an edge at that point.
	//*******************************************************************************
	Node * Tree::findPath(Node *node, int suffix, int beg,	int len) {

		if (len == 0) {
			printf("len is zero. why?\n");
		}

		Node *child = getChildWithFirstChar(node, input[beg]);

		if (child == nullptr) {
			Node *nchild = createNewNode(suffix, node, beg, len);
			addChildToNode(node, nchild);

			return nchild;
		}

		for (int i = 1; i < len; i++) {
			if (i >= child->len) {
				return findPath(child, suffix, beg + i, len - i);
			}
			if (input[beg + i] != input[child->beg + i]) {
				Node *mid = breakEdge(child, i);
				return findPath(mid, suffix, (beg + i), (len - i));
			}
		}

		panic("shouldnt actually happen");
	}

	//**********************************************************************
	//Straight forward inplementation of the 4 cases. findPath's first_char
	//is the position of the first character of V. Whcih is each suffix's
	//start position + the string depth (length of edge) upto v. findPath's
	//len is the edge length of the child that we need to find = length of 
	//sequence - suffix start position - the part of suffix covered upto v.
	//**********************************************************************
	Node * Tree::insert(Node *prevLeaf, int suffix) {
		Node *u = prevLeaf->parent;

		nLeaves++;

		if (prevLeaf == root) {
			return findPath(root, suffix, 0, input.length());

		} 
		else if (u->suffixLink != nullptr) {
			Node *v = u->suffixLink;
			//u->stringDepth is equivalent to the length of alpha
			return findPath(v, suffix, suffix + v->stringDepth,
				input.length() - suffix - v->stringDepth);

		} 
		else if (u->parent != root) {
			Node *uprime = u->parent;
			Node *vprime = uprime->suffixLink;
			int beta = u->beg;
			int betalen = u->len;

			Node *v = nodeHopTraversal(vprime, beta, betalen);
			u->suffixLink = v;

			return findPath(v, suffix, suffix + v->stringDepth,
				input.length() - suffix - v->stringDepth);

		} 
		else {
			Node *uprime = u->parent; 
			Node *vprime = uprime->suffixLink;
			int betaprime = u->beg + 1;
			int betaprimelen = u->len - 1;

			Node *v = nodeHopTraversal(vprime, betaprime, betaprimelen);
			u->suffixLink = v;

			return findPath(v, suffix, suffix + v->stringDepth,
				input.length() - suffix - v->stringDepth);
		}
	}

	//***************************************************
	//Traverse by jumping from one child node to the next 
	//***************************************************
	Node * Tree::nodeHopTraversal(Node *start, int beta, int len) {
		int i = 0, remaining;
		char c;
		Node *n1 = start, *n2;

		if (len == 0)
			return start;

		while (true) {
			remaining = len - i;
			c = input[beta + i];
			n2 = getChildWithFirstChar(n1, c);


			if (n2->len > remaining) {
				return breakEdge(n2, remaining);

			} 
			else if (n2->len == remaining) {
				return n2;

			} 
			else {
				i += n2->len;
				n1 = n2;
			}
		}
	}

	//*********************************************************************************
	//While hoping nodes or while doing find path, we might have to break edge. This 
	//function takes as input the number of positions, i, back from node n and breaks it
	//at that point.
	//*********************************************************************************
	Node * Tree::breakEdge(Node *n, int i) {
		Node *parent = nullptr, *nNode = nullptr;

		parent = n->parent;
		nNode = createNewNode(++nextNodeId, parent, n->beg, i);
		replaceChild(parent, nNode);

		n->sibling = nullptr;

		addChildToNode(nNode, n);

		n->beg = n->beg + i;
		n->parent = nNode;
		n->len = n->len - i;

		nIntNodes++;
		if (lowestIntNode == nullptr ||
			nNode->stringDepth > lowestIntNode->stringDepth) {
			lowestIntNode = nNode;
		}

		return nNode;
	}

	//**********************************************************
	//Getting the child of node n that has the fist character c.
	//**********************************************************
	Node * Tree::getChildWithFirstChar(Node *n, char c) {
		Node *cur = n->child;
		while (cur != nullptr)
		{
			if (input[cur->beg] == c)
				return cur;
			cur = cur->sibling;
		}
		return cur;
	}

	//*********************************************************************************
	//Adding a new child to a node. This has 3 cases: 1) when the node has no children,
	//2) When the child being added has $ then add it to the left, and 3) When child is
	//any other alphabet add it in the appropriate location.
	//*********************************************************************************
	void Tree::addChildToNode(Node *parent, Node *child) {
		Node *cur = parent->child;
		if (cur == nullptr) {
			parent->child = child;
			return;
		}

		char k = input[child->beg];
		if (k == '$') {
			if (cur != nullptr && input[cur->beg] == '$') {
				panic("shouldnt already have child with this label");
			}

			child->sibling = cur;
			parent->child = child;
			return;
		}

		Node *prev = nullptr;
		while(cur != nullptr) {
			if (k < input[cur->beg]) {
				child->sibling = cur;
				if (prev == nullptr) {
					parent->child = child;
				} else {
					prev->sibling = child;
				}
				return;
			}

			prev = cur;
			cur = cur->sibling;
		}
		prev->sibling = child;
	}

	//*****************************************************************************
	//Replace the child with node if they have the same first character. This child
	//node is what is being broken.
	//*****************************************************************************
	void Tree::replaceChild(Node *parent, Node *node) {
		Node *prev = nullptr;
		Node *cur = parent->child;
		char k = input[node->beg];

		while (cur != nullptr) {
			if (k == input[cur->beg]) {
				node->sibling = cur->sibling;
				if (prev == nullptr) {
					parent->child = node;
				} else {
					prev->sibling = node;
				}
				return;
			}

			prev = cur;
			cur = cur->sibling;
		}
	}

	//**********************************************************************************
	//This function creates a new node from the parent and sets the first character and
	//len (edge label) as the appropriate parameters.
	//**********************************************************************************
	Node *Tree::createNewNode(int id, Node *parent, int beg, int len) {
		Node *nxt = &alloc_buf[alloc_i++];
		nxt->id = id;
		nxt->parent = parent;
		nxt->beg = beg;
		nxt->len = len;
		nxt->EndLeafIndex = -1;
		nxt->StartLeafIndex = -1;
		if (parent != nullptr) {
			nxt->stringDepth = parent->stringDepth + len;
		}
		return nxt;
	}

	//***********************************************************************
    //Straight forward implementation of a reccursive DFS function. Deviates 
    //from DFS only in the en_parent_flag which says if parant (current node)
    //needs to be enumarated. So EnumerateChildrenNodesDFS sends false and 
    //NodeDFS sends true.
    //***********************************************************************
	void Tree::recursiveEnumerateNodesDFS(Node *n) {
		if (printCount == 10) {
			std::cout << std::endl;
			printCount = 0;
		}

		std::cout << n->stringDepth;

		if (printCount != 9)
			std::cout << " ";

		printCount++;
		Node *child = n->child;

		while (child != nullptr) {
			recursiveEnumerateNodesDFS(child);
			child = child->sibling;
		}
	}

	void Tree::EnumerateNodesDFS() {
		printCount = 0;
		recursiveEnumerateNodesDFS(root);
		std::cout << std::endl;
		printCount = 0;
	}

	//*********************************************************
    //Reccursive DFS that takes the left most child to get BWT.
    //*********************************************************
	void Tree::recursiveEnumerateBWT(Node *n) {
		if (n->child == nullptr) {													//Must be a leaf node.
			if (n->id == 0) {
				B[B_i++] = input[input.length() - 1];
			} else {
				B[B_i++] = input[n->id - 1];
			}
			return;
		}

		Node *child = n->child;
		while (child != nullptr) {
			recursiveEnumerateBWT(child);
			child = child->sibling;
		}
	}

	void Tree::EnumerateBWT()
	{
		recursiveEnumerateBWT(root);
		for (auto v : B) {
			std::cout << v << std::endl;
		}
	}

	void Tree::PrepareIndexArray() {
		int size = input.length() + 1;
		A = (int*) malloc(sizeof(int) * size);
		for (int i = 0; i < size; i++) {
			A[i] = -1;
		}

		nextIndex = 0;
		DFS_PrepareRoot(root);
	}

	void Tree::DFS_PrepareRoot(Node *nd) {
		if (nd == nullptr) {
			return;
		}

		if (nd->child == nullptr) {
			A[nextIndex] = nd->id;
			if (nd->stringDepth >= XValue) {
				nd->StartLeafIndex = nextIndex;
				nd->EndLeafIndex = nextIndex;
			}
			nextIndex++;
			return;
		}

		Node *child = nd->child;
		Node *last = nullptr;
		while (child != nullptr) {
			DFS_PrepareRoot(child);
			last = child;
			child = child->sibling;
		}

		if (nd->stringDepth >= XValue) {
			nd->StartLeafIndex = nd->child->StartLeafIndex;
			nd->EndLeafIndex = last->EndLeafIndex;
		}
	}

	Node * Tree::FindMatchPath(Node *node, const char *s, int *extra) {
		Node *child = getChildWithFirstChar(node, s[0]);
		*extra = 0;

		if (child == nullptr) {														//No child by that label
			return node;
		}

		int len = strlen(s);
		for (int i = 1; i < len; i++) {

			if (i >= child->len && child->child != nullptr) {
				return FindMatchPath(child, s+i, extra);
			}
			if (i >= child->len && child->child == nullptr) {
				panic("Hit a leaf~\n");
			}
			else if (s[i] != input[child->beg + i]) {
				*extra = i - 1;														//Backtrack up to internal node most recently visited.
				return node;
			}
		}
		
		return child;																//All suffix id's will belong to the child. 
	}

	std::vector<int> Tree::FindLoc(std::string& r) {
		std::vector<int> vec;
		int read_ptr = 0;
		const char *s = r.c_str();
		int len = r.length();
		int extra;																	//Extra bit of characters after a node
		int maxdepth = 0;
		Node *t = root;
		Node *deepestNode = t;
		Node *temp;

		while (read_ptr < len) {													//Find deepest node/longest substring
			temp = FindMatchPath(t, s + read_ptr, &extra);
			if (temp == root) {
				read_ptr++;
				continue;
			}
			read_ptr += temp->stringDepth - t->stringDepth;
			if (temp->stringDepth + extra > maxdepth) {
				deepestNode = temp;
				maxdepth = temp->stringDepth + extra;
			}
			t = temp->suffixLink;
		}

		if (maxdepth >= XValue) {
			for (int i = deepestNode->StartLeafIndex;
				i <= deepestNode->EndLeafIndex; i++) {
				vec.push_back(A[i]);
			}
		}

		return vec;
	}
}
