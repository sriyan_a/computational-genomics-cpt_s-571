#ifndef __MEASURESTATS_H__
#define __MEASURESTATS_H__

#include <ctime>
#include <cmath>
#include <list>
#include <iostream>
#include <string>

class Measure {
    public:
        Measure();
        ~Measure();
        void start();
        struct timespec next();
        struct timespec end();
        void stop();
        struct timespec avg();
		struct timespec std_dev();
		double total();
        
    private:
        bool running, paused;
        std::list<struct timespec> time_trials; 
        struct timespec ts_start, ts_end;
};


#endif
