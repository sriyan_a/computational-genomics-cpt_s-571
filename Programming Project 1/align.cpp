#include "align.h"
#include <string.h>
#include <iostream>
#include <cstring>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <math.h>
#include <climits>
#include <algorithm>

using namespace std;

//******************************************************************
//This class is used for the initialzations that are reruired accorss
//the class. I have following the naming convention according to the
//original paper sigma and epsilon, instead of h and g. This constructor
//allocated memory for S, D and I and another trace back matrix. It also
//fills the first row and column with the appropriate values
//*******************************************************************
Alignment::Alignment(const char *_target,const char *_read, int _sigma, int _epsilon, int _match, 
                        int _mismatch, int _pos_reads, const char *_sname, const char *_fname)
:sigma(_sigma), epsilon(_epsilon), match(_match), mismatch(_mismatch), pos_reads(_pos_reads)
{
    read = _read;
    target = _target;
    fname = _fname;
    sname = _sname;
    m = strlen(read) + 1;
    n = strlen(target) + 1;
    if (m > n) negative = INT_MIN - (sigma + 2*m*epsilon); //Over-engineered value.  A constant like
    else negative = INT_MIN - (sigma + 2*n*epsilon);       //-20000 has a chance of being overshot.
    
    V = new int *[m]; // Allocation for scoring matrix
    E = new int *[m];
    F = new int *[m];
    tracingBack = new char *[m]; // Allocation for retrace matrix
    for (i = 0; i < m; i++) {
        V[i] = new int[n];
        E[i] = new int[n];
        F[i] = new int[n];
        tracingBack[i] = new char[n];
    }

    // filling the first row, first column in the scoring matrix and backtracking matrix
    for (i = 0; i < m ; i++ ) {
        for (j = 0 ; j < n ; j++ ) {
            
            if (i == 0 && j == 0) {
                V[0][0] = 0;
                tracingBack[0][0] = 'n';
            }
            
            else if (i == 0) {
                V[0][j] = sigma + j*epsilon;
                E[0][j] = negative;
                F[0][j] = negative;
                tracingBack[0][j] = '-';
            }
            
            else if (j == 0) {
                V[i][0] = sigma + i*epsilon;
                F[i][0] = negative;
                E[i][0] = negative;
                tracingBack[i][0] = '|';
            }
        }
    }
}

//***************************************************************
//Straightforward destructor for everything that was constructed.
//Not very useful in this case because the class is alive till the
//end of the program but I have it here for future proofing.
//***************************************************************
Alignment::~Alignment() {

    for (i =0; i < m ; i++) {
        delete [] V[i];
        delete [] E[i];
        delete [] F[i];
        delete [] tracingBack[i];
    }

    delete [] V;
    delete [] E;
    delete [] F;
    delete [] tracingBack;
}

//**********************************************************************
//Recursive steps for the Needleman-Wunsch algorithm it also fills up the
//tracing back matrix that will make retrace very easy to code.
//**********************************************************************
void Alignment::global() {
    
    for (i = 1; i < m; i++) {
        for (j = 1; j < n; j++) {
            match_mismatch = read[i-1] == target[j-1] ?  match : mismatch;
            E[i][j] = max_global(E[i-1][j] + epsilon, F[i-1][j] + sigma + epsilon, V[i-1][j] + sigma + epsilon, &figure, false); // -
            F[i][j] = max_global(F[i][j-1] + epsilon, E[i][j-1] + sigma + epsilon, V[i][j-1] + sigma + epsilon, &figure, false); // |
            V[i][j] = max_global(V[i-1][j-1] + match_mismatch, E[i][j], F[i][j], &figure, true);
            tracingBack[i][j] = figure;
        }
    }
    score = V[m-1][n-1];
    
    retrace(m-1, n-1, "Global");
}

//**************************************************************************
//This function returns the optimal direction for global alignment, which ever 
//is the maximum and adds the appropriate charater to the trace back matrix. 
//This optimization saves us time later while retracing.
//**************************************************************************
int Alignment::max_global(int diagonal,  int vertical, int horizontal, char *figure, bool fig) {
    int  max = 0 ;
    
    if( diagonal > vertical && diagonal > horizontal ) {
        max = diagonal ;
        if (fig) { 
            *figure = '\\' ;
        }
    }
    
    else if (vertical > horizontal) {
        max = vertical ;
        if (fig) {
            *figure = '|' ;
        }
    }
    
    else {
        max = horizontal ;
        if (fig) {
            *figure = '-' ;
        }
    }
    return  max;
}

//*************************************************************************
//Recursive steps for the Smith-Waterman algorithm similar to global alignment
//*************************************************************************
void Alignment::local() {
    std::string alignRef, alignTarget, dots;

    score = negative;
    for (i = 1; i < m; i++) {
        for (j = 1; j < n; j++) {
            match_mismatch = read[i-1] == target[j-1] ?  match : mismatch;
            E[i][j] = max_local(E[i-1][j] + epsilon, F[i-1][j] + sigma + epsilon, V[i-1][j] + sigma + epsilon, 0, &figure, false); // -
            F[i][j] = max_local(F[i][j-1] + epsilon, E[i][j-1] + sigma + epsilon, V[i][j-1] + sigma + epsilon, 0, &figure, false); // |
            V[i][j] = max_local(V[i-1][j-1] + match_mismatch, E[i][j], F[i][j], 0, &figure, true);
            if ( V[i][j] >= score ) {
                score = V[i][j];
                indexI = i;
                indexJ = j;
            }
            tracingBack[i][j] = figure;
        }
    }
    retrace(indexI, indexJ, "Local");
}

//**********************************************************************************
//Performs retracing by traversing the charaters added to the trace back matrix. This
//matix, upon pring, yeilds a very beautiful figure. 
//**********************************************************************************
void Alignment::retrace(int i, int j, const char *gl) {
    std::string alignRef, alignTarget, dots;
    int indexJ, opening_gaps = 0, gaps = 0, matches = 0, mismatches = 0;

    indexJ = j;

    while ( tracingBack[i][j] != 'n' ) {
        
        switch(tracingBack[i][j]) {
                
            case '\\' :
                if (alignTarget[0] == '-' || alignRef[0] == '-') opening_gaps++;
                alignTarget = read[i-1] + alignTarget;
                alignRef = target[j-1] + alignRef;
                if (read[i-1] == target[j-1]) {
                    dots = '|' + dots;
                    matches++;
                }
                else {
                    dots = '.' + dots;
                    mismatches++;
                }
                i--;
                j--;
                break;
                
            case '|' :
                gaps++;
                alignTarget = read[i-1] + alignTarget;
                alignRef = '-' + alignRef;
                dots = ' ' + dots;
                i--;
                break;
                
            case '-' :
                gaps++;
                alignTarget = '-' + alignTarget;
                alignRef = target[j-1] + alignRef;
                dots = ' ' + dots;
                j--;
        }
    }

    for ( j = indexJ  ; j < n-1 ; j++ ) {
        alignRef += target[j];
        alignTarget += '-';
    }

    print_output(gl, alignRef, alignTarget, dots, opening_gaps, gaps, matches, mismatches, score);
}

//****************************************************************************************
//Similar to max_global, returns the optimal step to take. But now 0 is another parameter.
//This also fills us the tracing back matrix with the right direction.
//****************************************************************************************
int Alignment::max_local(int diagonal,  int vertical, int horizontal, int zero, char *figure, bool fig) {
    int  max = 0;
    
    if( diagonal > vertical && diagonal > horizontal && diagonal > zero) {
        max = diagonal ;
        if(fig) { 
            *figure = '\\';
        }
    }
    
    else if (vertical > horizontal  && vertical > zero) {
        max = vertical ;
        if(fig) { 
            *figure = '|';
        }
    }
    
    else if (horizontal > zero) {
        max = horizontal ;
        if(fig) {
            *figure = '-';
        }
    }

    else {
        max = 0;
        if (fig) {
            *figure = 'n';
        }
    }
    return  max ;
}

//******************************************************************************************************************************
//This over-engineered function is assignment specific and prints the output is the format that is required for my submission.
//For this we only need how long each line is in the file, so that the same length can be used to print each line of the output.
//******************************************************************************************************************************
void Alignment::print_output(std::string gl, std::string alignRef, std::string alignTarget, 
                                std::string dots, int opening_gaps, int gaps, int matches, int mismatches, int score) {
    int ini_read, fin_read, ini_target, fin_target, ini;
    std::string refPrint, refPrint_read, refPrint_target;
    
    ini_read = 0;
    fin_read = pos_reads;
    ini_target = 0;
    fin_target = pos_reads;
    i = 0;

    cout << "\n***  " << gl + " Alignment" << "  ***" << endl;
    cout << "Scores: match = " << match << ", mistmatch = " << mismatch << ", h = " << sigma << ", g = " << epsilon << endl;
    cout << "Sequence 1 = \"" << fname << "\", length = " << n-1 << " characters" << endl;
    cout << "Sequence 2 = \"" << sname << "\", length = " << m-1 << " characters" << endl;
    
    dots.append(alignRef.length() - dots.length(), ' ');

    while(true) {

        if ((fin_read <= max(m, n)) && (fin_target <= max(m, n))) {
            refPrint_read = alignRef.substr(i, pos_reads);
            fin_read -= std::count(refPrint_read.begin(), refPrint_read.end(), '-');
            if ((gl == "Local") && (ini_read >= fin_read)) break;
            
            refPrint = dots.substr(i, pos_reads);

            refPrint_target = alignTarget.substr(i, pos_reads);
            fin_target -= std::count(refPrint_target.begin(), refPrint_target.end(), '-');
            if ((gl == "Local") && (ini_target >= fin_target)) break;
            
            cout << "S1: " + to_string(ini_read) + ' ' << refPrint_read << ' ' + to_string(fin_read) << endl;
            cout << "    " + std::string( to_string(ini_read).length()+1, ' ' ) << refPrint << endl;
            cout << "S2: " + to_string(ini_target) + 
            std::string( to_string(ini_read).length() - to_string(ini_target).length() + 1, ' ' ) 
            << refPrint_target << ' ' + to_string(fin_target) << "\n" << endl;

            i += pos_reads;
            ini_read = fin_read + 1;
            ini_target = fin_target + 1;
            fin_read += pos_reads;
            fin_target += pos_reads;
        }

        else {
            refPrint_read = alignRef.substr(i, floor(fin_read - fin_target));
            fin_read = ini_read + refPrint_read.length() - std::count(refPrint_read.begin(), refPrint_read.end(), '-') - 1;
            if ((gl == "Local") && (ini_read >= fin_read)) break;
            
            refPrint = dots.substr(i, floor(fin_read - fin_target));

            refPrint_target = alignTarget.substr(i, floor(fin_read - fin_target));
            fin_target = ini_target + refPrint_target.length() - std::count(refPrint_target.begin(), refPrint_target.end(), '-') - 1;
            if ((gl == "Local") && (ini_target >= fin_target)) break;

            cout << "S1: " + to_string(ini_read) + ' ' << refPrint_read << ' ' + to_string(fin_read) << endl;
            cout << "    " + std::string( to_string(ini_read).length()+1, ' ' ) << refPrint << endl;
            cout << "S2: " + to_string(ini_target) + 
            std::string( to_string(ini_read).length() - to_string(ini_target).length() + 1, ' ' ) 
            << refPrint_target << ' ' + to_string(fin_target) << "\n" << endl;
            break;
        }
    }

    cout << "Report: \nScore is: " << score << endl;
    cout << "This is a test to see if retrace is correct, score should equal " << match*matches+mismatch*mismatches+(epsilon*(gaps))+(sigma*opening_gaps) << endl;  //Unable to get this to match the score.. TODO
    cout << "matches = " << matches << ", mismatches = " << mismatches << ", gaps = " << gaps << ", opening gaps = "<< opening_gaps << endl;
    max_len = matches + mismatches + gaps;
    cout << "Identities = " << matches << '/' << max_len << " = " << 100.0*(float)matches/(float)max_len << 
            "\%, Gaps = " << gaps << '/' << max_len << "= " << 100.0*(float)gaps/(float)max_len << "\%" << endl;
}
