#include "align.h"
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <fstream>

using namespace std;

int is_local, m_a, m_i, g, h;
string target, read, first_name, second_name;
int pos_targets = 0, pos_reads = 0;

int get_data(const char *argv[]){
    
    char c, temp, line[32], c_temp;
    char inputpath[64], configpath[64] = "parameters.config";
    int first = 0, pos_target = 0, pos_read = 0, count;
    bool skip_line = false, get_gene_flag= true;

    //reading the input file for the two genomes.
    if ( argv[1] && argv[2] && ( *argv[2] == '0' || *argv[2] == '1' )) {
		strncpy(inputpath, argv[1], 64);
		strncpy(&temp, argv[2], 1);
		is_local = temp - '0';
	}

    ifstream is(inputpath); // open file

    //reading file character by character because need to check for name and the number of characters in a line.
    //Initially built so that lengths of each line can be added to an array, which is used at the time of printing.
    //But it was later foundout that the programmer that it was not required and only one line length is requried.
    //TODO: Do this more efficiently line by line.
    while (is.get(c)) { // loop getting single characters
        if (get_gene_flag) {
            c_temp = c;
            c = '>';
        }
        switch(c) {
            case '>': {
                first++;
                if(get_gene_flag == false) get_gene_flag = true;
                if (c_temp == ' ') {
                    get_gene_flag = false;
                    skip_line = true;
                    c_temp = '>';           //HACK: Char doens't forget the space for the next time.
                }                           //Even setting it as \0 leaves the space. (Weird!)
                if (get_gene_flag) {
                    if(c_temp != '>') {
                        if(first == 1) first_name += c_temp;
                        else if (first == 2) second_name += c_temp;
                    }
                    first--;
                }
                count = 1;
                break;
            }
            case 'a': case 't': case 'c': case 'g': case 'A': case 'T': case 'C': case 'G': {
                if (!skip_line) {
                    if(first == 1) {
                        target += c;
                        pos_target++;
                    }
                    else if(first == 2) {
                        read += c;
                        pos_read++;
                    }
                }
                break;
            }
            case '\n': {
                if (skip_line) {
                    skip_line = false;
                }
                else if(first == 1 && pos_targets == 0) {
                    pos_targets = pos_target;
                }
                else if(first == 2 && pos_reads == 0)  {
                    pos_reads = pos_read;
                }
                break;
            }
            default: break;
        }
    }

    is.close(); // close file

    if(argv[3]) strncpy(configpath, argv[3], 64);
    
    //Read the config file for the parameters needed to run the algorithms. This is done line by line 
    //as it is not required to a sequence of characters until a space like before.
    FILE *fp = fopen(configpath, "r");
    if(fp)
        while(fgets(line, 32, fp)) {
            char *tmp = strtok(line, " \t\n");
            if(strcmp(tmp, "match") == 0) {
                tmp = strtok(NULL, "\t\n");
                m_a = stoi(tmp);
            }
            else if(strcmp(tmp, "mismatch") == 0) {
                tmp = strtok(NULL, " \t\n");
                m_i = stoi(tmp);
            }
            else if(strcmp(tmp, "h") == 0) {
                tmp = strtok(NULL, " \t\n");
                h = stoi(tmp);
            }
            else if(strcmp(tmp, "g") == 0) {
                tmp = strtok(NULL, " \t\n");
                g = stoi(tmp);
            }
        }

    fclose(fp);
    return 0;
}

int main( int argc, const char *argv[] ) {
    
    if ( argc < 3 ) {
        cout << "Error!!! Format is: \n";
        cout << "$<executable name> <input file containing both s1 and s2> <0: global, 1: local> <optional: path to parameters config file>\n";
        return -1;
    }
        
    if (get_data(argv)) return -1;
    
    Alignment align(target.c_str(), read.c_str(), h, g, m_a, m_i, 
                    pos_reads, second_name.c_str(), first_name.c_str());
    
    if (!is_local) align.global();
    else align.local();
    
    return 0;
}