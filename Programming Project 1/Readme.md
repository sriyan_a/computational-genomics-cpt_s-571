Steps to execute:

1) g++ align.h
2) g++ -c main.cpp align.cpp
3) g++ main.o align.o -o output
4) output.exe <input file containing both s1 and s2> <0: global, 1: local> <optional: path to parameters config file>

This project was done as a part of CPT_S 571. This project implements Needleman-Wunsch algorithm for computing OPTIMAL GLOBAL ALIGNMENT and Smith-Waterman algorithm for computing OPTIMAL LOCAL ALIGNMENT, with affine gap penalty function between two input DNA sequences, s1 and s2, of lengths m and n respectively.

Config file should be of the format:
    match    1
    mismatch    -2
    h  -5 
    g -1

And an example for the input file is as given below:

    >s1 sequence
    acatgctacacgtactccgataccccgtaaccgataacgatacacagacct
    cgtacgcttgctacaacgtactctataaccgagaacgattgaca
    tgcctcgtacacatgctacacgtactccgatgaccccgt

    >s2 sequence
    acattctacgaacctctcgataaccccataaccgataacgattgacacctcgt
    acgctttctacaacttactctctcgataaccccataaccgataacgattgacacctc
    gtacacatggtacatacgtactctcgataccccgt

