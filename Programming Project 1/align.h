#include <cstddef>
#include <string>

#ifndef AL_H
#define AL_H

class Alignment {
    
    private: 
		int  score, match_mismatch, i, j, k, m, n, max_len, indexI, indexJ, indexK, sigma, epsilon, match, mismatch;
		int negative, gaps, matches, mismatches, ini, fin, pos_reads;
		float indentities, gap_per;
		const char *read, *target, *fname, *sname;
		int  **V, **G, **E, **F;
		char **tracingBack;
		char figure;
		
		int max_global(int diagonal,  int vertical, int horizontal, char *figure, bool fig) ;
		
		int max_local(int diagonal,  int vertical, int horizontal, int zero, char *figure, bool fig) ;

		void print_output(std::string gl, std::string alignRef, std::string alignTarget, std::string dots, 
								int opening_gaps, int gaps, int matches, int mismatches, int score);
		
		void retrace(int i, int j, const char *gl);

    public:
		Alignment(const char *_target, const char *_read, int _sigma, int _epsilon, int _match, int _mismatch, 
						int _pos_reads, const char *_sname, const char *_fname);
		~Alignment();
		void global();
		void local();
};

#endif