This project has been tested only with MinGW C++ in Windows 64bit environment. In other environments changes to the getPeakMem() and std::chrono::high_resolution_clock::now() might not be the only ones that are required.

Use the followig command in the src directory:
``runcode.bat``

You will find the outputs in the ``outputs`` folder. Also, it is to be noted that if the user doesn't wish to see the outputs of the DFS traversal, they are to comment steps (ii) and (iii) in the main function.

System Configuration Recommended for replication of results:
* Processor: Intel(R) Core(TM) i5-8250U CPU @ 1.60GHz, 1800 Mhz, 4 Core(s), 8 Logical Processor(s)
* Installed Physical Memory (RAM): 12.0 GB
* Total Physical Memory: 11.9 GB
* Total Virtual Memory: 19.4 GB
* Page File Space: 7.50 GB
* L2CacheSize: 1024 bytes
* L3CacheSize: 6144 bytes
