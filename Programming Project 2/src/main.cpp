#include "suffix_tree.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <time.h>
#include <windows.h>
#include <psapi.h>
#include <chrono>

using namespace std;

string ibuff, abuff;
string iname;
int len;

int get_data(const char * argv[]) {
    
    string inputname, alphaname;
    char inchar;

    inputname = argv[1];
	alphaname = argv[2];

    ifstream alphafile;
    alphafile.open(alphaname, ios::in);

    while (!alphafile.eof()) {
	    alphafile.get(inchar);
        if (inchar != ' ' && inchar != '\n')
        abuff += inchar;
	}
    abuff += '$';

    ifstream infile;
    infile.open(inputname, ios::in);

    bool flag = false;

	do {
		infile.get(inchar);
		if (inchar == '>') flag = true;
        else if (inchar == '\n') flag = false;
        else iname += inchar;
	} while (flag);

    ifstream infile2;
    infile2.open(inputname, ios::in);
    flag = false;

    len = 0;
	do {
		infile2.get(inchar);
        if (flag) {
            if (inchar != ' ' && inchar != '\n')
                ibuff += inchar;
                len++;
        }
        else {
            if (inchar == '\n')
                flag = true;
        }
	} while (!infile2.eof());

	infile.close();
	infile2.close();
	alphafile.close();

	return (0);
}

size_t getPeakMem() {
    auto myHandle = GetCurrentProcess();
    PROCESS_MEMORY_COUNTERS pmc;
    GetProcessMemoryInfo(myHandle, &pmc, sizeof(pmc));
    return(pmc.PeakWorkingSetSize);
}

int main( int argc, const char *argv[] ) {
    
    if ( argc < 3 ) {
        cout << "Error!!! Format is: \n";
        cout << "$<executable name> <input file containing sequence> <input file with alphabet>\n";
        return -1;
    }
    
    if (get_data(argv)) return -1;
    
    suffixtree::Tree *t = new suffixtree::Tree(ibuff, abuff);
    SYSTEMTIME tim1, tim2;

    cout << "\ni) Building the tree..." << endl;

    auto start = std::chrono::high_resolution_clock::now();
	suffixtree::Node *root = t->Build();
    size_t end_mem = getPeakMem();
    auto elapsed = std::chrono::high_resolution_clock::now() - start;
    long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();

    cout << "\nii) Enumarating Children Nodes from root (for output puposes, will work with any node) using DFS traversal:" << endl;
	t->EnumerateChildrenNodesDFS(root);

    cout << "\niii) Enumarating Nodes using DFS traversal:" << endl;
	t->NodeDFS();

    cout << "\niv) Enumarating BWT:" << endl;
    t->GetBWTSrting();

    cout << "\nv) Printing Tree's information, memory and time usage for testing and analysis:" << endl;
	t->PrintRequiredInfo();
    cout << "Time taken to build the tree (in microsec): " << microseconds << endl;
    cout << "Memory taken to build the tree (bytes): " << end_mem << endl;
    long input_mem = (ibuff.length()*sizeof(string)) + (abuff.length()*sizeof(string));
    cout << "Input size, input sequence + alphabet, (bytes): " << input_mem << endl;
    cout << "Input to peak usage ratio: " << (float)end_mem/input_mem << endl;

    return 0;
}