#include "suffix_tree_node.h"

namespace suffixtree {
    Node::Node( int id, Node *parent, int first_char, int len)
    :id(id), first_char(first_char), len(len), parent(parent), suffixLink(nullptr), child(nullptr), sibling(nullptr) {

        if (parent != nullptr) {
            stringDepth = parent->stringDepth + len;
        }
    }
}