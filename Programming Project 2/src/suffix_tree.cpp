#include "suffix_tree.h"
#include <string.h>
#include <iostream>

namespace suffixtree {

    Tree::Tree(std::string& sequence, std::string alphabet)
    :sequence(sequence), alphabet(alphabet), BWT(sequence.length() + 1, 0), BWT_i(0), 
    nIntNodes(1), nLeaves(0), sumStringDepths(0) {
        
        sequence += '$';
        input_len = sequence.length();

        size_t size = sizeof(Node) * input_len * 2;
        allocated_buffer = (Node*)malloc(size);
        memset(allocated_buffer, '\0', size);

        alloc_i = 0;

        nextNodeId = input_len;
        root = createNewNode(nextNodeId++, nullptr, 0, 0);
        deepestIntNode = root;
        root->suffixLink = root;
    }

    Node * Tree::Build() {

        Node *n = root;
        for (int i = 0; i < input_len; i++) {                                           //Given pseudo code: insert all suffixes
            n = insert(n, i);
        }

        return root;
    }

    //**********************************************************************
    //Straight forward inplementation of the 4 cases. findPath's first_char
    //is the position of the first character of V. Whcih is each suffix's
    //start position + the string depth (length of edge) upto v. findPath's
    //len is the edge length of the child that we need to find = length of 
    //sequence - suffix start position - the part of suffix covered upto v.
    //**********************************************************************
    Node *Tree::insert(Node *prevLeaf, int suffix) {

        Node *u = prevLeaf->parent;
        nLeaves++;

        if (prevLeaf == root)
            return findPath(root, suffix, 0, input_len);
        
        else if (u->suffixLink != nullptr) {                                            //Case 1: u already existed at the start of iteration (suffix-1).
            Node *v = u->suffixLink;
            return findPath(v, suffix, suffix + v->stringDepth,
                input_len - suffix - v->stringDepth);
        }

        else if (u->parent != root) {                                                   //Case 2A: u was created during iteration (suffix-1), and u' is not root
            Node *uprime = u->parent, *vprime = uprime->suffixLink;
            int beta = u->first_char;                                                   //Beta is the edge label between u and uprime.
            int betalen = u->len;

            Node *v = nodeHopTraversal(vprime, beta, betalen);
            u->suffixLink = v;

            return findPath(v, suffix, suffix + v->stringDepth,
                input_len - suffix - v->stringDepth);
        }
        
        else {                                                                          //Case 2B: u was created during iteration (suffix-1), and u' is the root
            Node *uprime = u->parent, *vprime = uprime->suffixLink;                     // also the root
            int betaprime = u->first_char + 1;                                          // beta = x betaprime
            int betaprimelen = u->len - 1;

            Node *v = nodeHopTraversal(vprime, betaprime, betaprimelen);
            u->suffixLink = v;

            return findPath(v, suffix, suffix + v->stringDepth,
                input_len - suffix - v->stringDepth);
        }
    }

    //*******************************************************************************
    //Find path first checks if there is a child. If there is no child, that means we
    //need to add a new node (child) at that point. If child exists, we check it's
    //edge length with our len, if it is greater then that means we can hop one node.
    //If the len is less than child's edge len then that means our suffix finishes
    //mid way somewhere along the edge. So we break an edge at that point.
    //*******************************************************************************
    Node *Tree::findPath(Node *node, int suffix, int first_char, int len) {

        Node *child = getChildWithFirstChar(node, sequence[first_char]);

        if (child == nullptr) {                                                         // No child by that label
            Node *nchild = createNewNode(suffix, node, first_char, len);
            addChildToNode(node, nchild);
            return nchild;
        }

        for (int i = 1; i < len; i++) {

            if (i >= child->len)
                return findPath(child, suffix, first_char + i, len - i);

            if (sequence[first_char + i] != sequence[child->first_char + i]) {
                Node *mid = breakEdge(child, i);
                return findPath(mid, suffix, (first_char + i), (len - i));
            }
        }

        std::cout << "Program flow shouldn\'t reach this part! If it does, I don't know what to do.\n";
        exit(1);
    }

    //***************************************************
    //Traverse by jumping from one child node to the next 
    //***************************************************
    Node *Tree::nodeHopTraversal(Node *start, int beta, int len) {
        int i = 0, remaining;
        char c;
        Node *n1 = start, *n2;

        if (len == 0)
            return start;

        while (true) {
            remaining = len - i;
            c = sequence[beta + i];
            n2 = getChildWithFirstChar(n1, c);

            if (n2->len > remaining)                                                    //break edge right before index 'remaining' of the edge.
                return breakEdge(n2, remaining);
        else if (n2->len == remaining)                                                  //Beta is perfectly consumed, we have found V.
                return n2;
            else {                                                                      //Travel edge, consume part of beta
                i += n2->len;
                n1 = n2;
            }
        }
    }

    //*********************************************************************************
    //While hoping nodes or while doing find path, we might have to break edge. This 
    //function takes as input the number of positions, i, back from node n and breaks it
    //at that point.
    //*********************************************************************************
    Node *Tree::breakEdge(Node *n, int i) {
        Node *parent = nullptr, *nNode = nullptr;

        parent = n->parent;
        nNode = createNewNode(nextNodeId++, parent, n->first_char, i);                  //Make new node
        replaceChild(parent, nNode);                                                    //Remove n from parent's children, replace with new node.

        n->sibling = nullptr;
        addChildToNode(nNode, n);                                                       //Insert n under new node

        n->first_char = n->first_char + i;
        n->parent = nNode;
        n->len = n->len - i;

        nIntNodes++;                                                                    //All of this is for printing tree info
        sumStringDepths += nNode->stringDepth;
        if (deepestIntNode == nullptr || nNode->stringDepth > deepestIntNode->stringDepth)
            deepestIntNode = nNode;

        return nNode;
    }

    //**********************************************************************************
    //This function creates a new node from the parent and sets the first character and
    //len (edge label) as the appropriate parameters.
    //**********************************************************************************
    Node *Tree::createNewNode(int id, Node *parent, int first_char, int len) {
        Node *new_node = &allocated_buffer[alloc_i++];
        new_node->id = id;
        new_node->parent = parent;
        new_node->first_char = first_char;
        new_node->len = len;
        if (parent != nullptr)
            new_node->stringDepth = parent->stringDepth + len;
        
        return new_node;
    }

    //*****************************************************************************
    //Replace the child with node if they have the same first character. This child
    //node is what is being broken.
    //*****************************************************************************
    void Tree::replaceChild(Node *parent, Node *node) {
        Node *prev = nullptr;
        Node *curr = parent->child;
        char k = sequence[node->first_char];

        while (curr != nullptr) {

            if (k == sequence[curr->first_char]) {
                node->sibling = curr->sibling;
                if (prev == nullptr)
                    parent->child = node;
                else
                    prev->sibling = node;
                
                return;
            }

            prev = curr;
            curr = curr->sibling;
        }
    }

    //**********************************************************
    //Getting the child of node n that has the fist character c.
    //**********************************************************
    Node *Tree::getChildWithFirstChar(Node *n, char c) {

        Node *curr = n->child;
        while (curr != nullptr) {

            if (sequence[curr->first_char] == c)
                return curr;
            curr = curr->sibling;
        }

        return curr;
    }

    //*********************************************************************************
    //Adding a new child to a node. This has 3 cases: 1) when the node has no children,
    //2) When the child being added has $ then add it to the left, and 3) When child is
    //any other alphabet add it in the appropriate location.
    //*********************************************************************************
    void Tree::addChildToNode(Node *parent, Node *child) {
        Node *curr = parent->child;

        if (curr == nullptr) {
            parent->child = child;
            return;
        }

        char k = sequence[child->first_char];

        if (k == '$') {
            child->sibling = curr;
            parent->child = child;
            return;
        }

        Node *prev = nullptr;
        while(curr != nullptr) {

            if (k < sequence[curr->first_char]) {

                child->sibling = curr;
                if (prev == nullptr)
                    parent->child = child;
                else
                    prev->sibling = child;
                return;
            }

            prev = curr;
            curr = curr->sibling;
        }
        prev->sibling = child;
    }

    //***********************************************************************
    //Straight forward implementation of a reccursive DFS function. Deviates 
    //from DFS only in the en_parent_flag which says if parant (current node)
    //needs to be enumarated. So EnumerateChildrenNodesDFS sends false and 
    //NodeDFS sends true.
    //***********************************************************************
    void Tree::_DFS(Node *n, bool en_parent_flag) {

        if (en_parent_flag) {
            if (printCount == 10) {                                                     //After 10 enumerations, print a newline
                std::cout << std::endl;
                printCount = 0;
            }

            std::cout << n->stringDepth;

            if (printCount != 9)
                std::cout << " ";

            printCount++;
        }
        Node *child = n->child;

        while (child != nullptr) {
            _DFS(child, true);
            child = child->sibling;
        }
    }

    void Tree::NodeDFS() {

        printCount = 0;
        _DFS(root, true);
        std::cout << std::endl;
        printCount = 0;
    }

    void Tree::EnumerateChildrenNodesDFS(Node *n) {

        printCount = 0;
        _DFS(n, false);
        std::cout << std::endl;
        printCount = 0;
    }

    //*********************************************************
    //Reccursive DFS that takes the left most child to get BWT.
    //*********************************************************
    void Tree::_BWT(Node *n) {
        
        if (n->child == nullptr) {                                                      //Must be a leaf node.
            if (n->id == 0)
                BWT[BWT_i++] = sequence[input_len - 1];
            else
                BWT[BWT_i++] = sequence[n->id - 1];
            return;
        }

        Node *child = n->child;
        while (child != nullptr) {
            _BWT(child);
            child = child->sibling;
        }
    }

    void Tree::GetBWTSrting() {
        _BWT(root);
        for (auto v : BWT) {
            std::cout << v << std::endl;
        }
    }

    void Tree::PrintRequiredInfo() {
        std::cout << "Number of leaves: " << nLeaves << std::endl;
        std::cout << "Number of internal nodes (including root): " << nIntNodes << std::endl;
        int totalNodes = nLeaves + nIntNodes;
        std::cout << "Total number of nodes: " << totalNodes << std::endl;
        std::cout << "Average sring depth of all internal nodes are: " << 
                                        sumStringDepths/nIntNodes << std::endl;
        std::cout << "Size of tree in bytes: " << sizeof(deepestIntNode)*totalNodes 
                                                                    << std::endl;
        int deepLen = deepestIntNode->stringDepth;
        std::cout << "Depth of deepest internal node (length of longest repeatingsubstring): " 
                                                                    << deepLen << std::endl;
        std::cout << "Starting indidies of the longest repeating sequence: ";

        Node *child = deepestIntNode->child;
        int idx;

        while (child != nullptr) {
            idx = child->id;
            std::cout << idx+1 << " ";                                                  //Child node id (leaf) is the same
            child = child->sibling;                                                     //as the edge start index.
        }
        std::cout << std::endl;

        std::cout << "Longest repeating subsequence is: ";
        deepLen += idx;
        while(idx < deepLen) {
            std::cout << sequence[idx];
            idx++;
        }
        std::cout << std::endl;

        std::cout << "Length of sequence: " << input_len << std::endl;
    }
}