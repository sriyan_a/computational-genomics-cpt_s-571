#ifndef SUFFIX_TREE_H
#define SUFFIX_TREE_H

#include <string>

#include "suffix_tree_node.h"

namespace suffixtree {

    class Tree {
        private:
            Node *allocated_buffer, *root, *deepestIntNode;
            int alloc_i, alphabetSize, nextNodeId, printCount, BWT_i, nIntNodes, nLeaves, input_len, sumStringDepths;
            std::string alphabet, &sequence;
            bool ranDFS;

            Node *insert(Node *prevLeaf, int suffix);
            Node *nodeHopTraversal(Node *start, int beta, int len);
            Node *getChildWithFirstChar(Node *n, char c);
            void addChildToNode(Node *parent, Node *child);
            void replaceChild(Node *parent, Node *newChild);
            void _DFS(Node *n, bool en_parent_flag);
            void _BWT(Node *n);
            Node *breakEdge(Node *n, int i);
            Node *findPath(Node *node, int suffix, int first_char, int len);
            Node *createNewNode(int id, Node *parent, int first_char, int len);

        public:
            std::vector<char> BWT;
            Tree(std::string& sequence, std::string alphabet);
            Node *Build();
            void NodeDFS();
            void EnumerateChildrenNodesDFS(Node *n);
            void GetBWTSrting();
            void PrintRequiredInfo();
    };

}

#endif