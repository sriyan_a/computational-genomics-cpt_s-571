#ifndef SUFFIX_TREE_NODE_H_
#define SUFFIX_TREE_NODE_H_
#include <string>
#include <vector>

namespace suffixtree {
    class Tree;
    class Node {

        friend Tree;

        public:
            Node(int id, Node *parent, int first_char, int len);
            void AddChild(Node *node);

            Node *parent, *child, *sibling, *suffixLink;
            int first_char, len, id, stringDepth;
    };
}

#endif